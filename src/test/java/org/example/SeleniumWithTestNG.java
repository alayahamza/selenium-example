package org.example;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class SeleniumWithTestNG {

    private SeleniumExample seleniumExample;

    @BeforeSuite
    public void setUp() {
        seleniumExample = new SeleniumExample();
    }

    @AfterSuite
    public void tearDown() {
        seleniumExample.closeWindow();
    }

    @Test
    public void should_return_page_title_when_search() {
        seleniumExample.getPage();
        String actualTitle = seleniumExample.getTitle();
        String expectedTitle = "stack overflow - Recherche Google";
        assertEquals(actualTitle, expectedTitle);
    }
}
