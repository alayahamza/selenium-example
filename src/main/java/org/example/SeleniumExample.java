package org.example;

import org.example.config.SeleniumConfig;
import org.openqa.selenium.WebElement;

public class SeleniumExample {

    private final SeleniumConfig config;

    public SeleniumExample() {
        config = new SeleniumConfig();
        config.navigateTo("http://www.google.com");

    }

    public void closeWindow() {
        this.config.close();
    }

    public void getPage() {
        WebElement searchInput = this.config.findElementByName("q");
        this.config.clickWebElement(searchInput);
        this.config.typeSearchCriteria(searchInput);
        WebElement searchButton = this.config.findElementByName("btnK");
        this.config.clickWebElement(searchButton);
    }

    public String getTitle() {
        return this.config.getTitle();
    }
}
