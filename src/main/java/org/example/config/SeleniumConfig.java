package org.example.config;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class SeleniumConfig {

    private final WebDriver driver;

    public SeleniumConfig() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public void close() {
        driver.close();
    }

    public void navigateTo(String url) {
        driver.navigate().to(url);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement findElementByName(String name) {
        return getDriver().findElement(By.name(name));
    }

    public void clickWebElement(WebElement webElement) {
        webElement.click();
    }

    public void typeSearchCriteria(WebElement webElement) {
        webElement.sendKeys("stack overflow");
    }

    public String getTitle() {
        return getDriver().getTitle();
    }
}
